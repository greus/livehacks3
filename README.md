# Team 2 - soundflow

![logo](logo.png)

### Websocket channel

`ws://stagecast.se/api/events/lh3_team02/ws?x-user-listener=1`

### Credits
Music: https://www.bensound.com

Pulse: https://codepen.io/ialphan/pen/DecdC
